void printstats(struct stat stat);

int main(int argc, char *argv[]){
    FILE *f;

    struct stat stats;

    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }

    f = fopen(argv[1], "r");
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }

    if(stat(argv[1], &stats) != 0){
        perror("Unable to get file properties");
        exit(EXIT_FAILURE);
    }

    printstats(stats);

    fclose(f);
    exit(EXIT_SUCCESS);
}

void printstats(struct stat stat) {
    printf("EGID = %u\n", stat.st_gid);
    printf("EUID = %u\n", stat.st_uid);
}
