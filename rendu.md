# Rendu "Les droits d’accès dans les systèmes UNIX"

## Monome

- Nom, Prénom, email: Romane MERVEAUX romane.merveaux.etu@univ-lille.fr

## Question 1

Réponse :

Instructions :
    addgroup ubuntu
    adduser toto
    adduser toto ubuntu

Ici le fichier est a toto mais aussi au groupe de toto (ubuntu) qui a droit à "r" et "w" c'est à dire lecture et écriture

## Question 2

Réponse : Le "x" pour un répertoire autorise l'accés ou l'ouverture du répertoire.
Instructions :
    su toto
    cd toto
    mkdir mydir

Intruction pour enlever les droits :
    chmod g=rw mydir/
    
Ainsi toto n'a plus l'accés a mydir, à l'aide de ls -l on s'appercoit que le groupe ubuntu n'a pas le droit à l'exécution. Et donc par extension pas toto.

Instruction création du fichier :
    touch data.txt
    
Toto n'a pas le droit "x" il ne peut donc pas voir l'arborescence de mydir, et ni les fichiers.

## Question 3

Réponse : voir fichiers
En exécutant avec toto :

les IDs : 
EUID : 1001
EGID : 1001
RUID : 1001
RGID : 1001

Le fichier ne peux pas être ouvert. Permission refusé.

En exécutant avec toto sans le chmod u+p :

Les IDs :
EUID : 1000
EGID : 1001
RUID : 1001
RGID : 1001

Le processus à en effet l'accés au fichier, et on remarque que le droit d'éxecution du processus éxecutant est similaire à celui de ubuntu.

## Question 4

Réponse : voir fichiers

En exécutant avec toto :



En exécutant avec toto sans le chmod u+p :




## Question 5

Réponse : Cette commande permet de modifier ses informations personnelles. Ou celles d'autres utilisateurs


Le résultat : -rwsr-xr-x 1 root root 84848 août  29  2019
chfn est le fichier de la commande associée
On voit bien que le root peut lire écrire, et exécuter le fichier chfn. Le groupe root lui peut éxecuter et lire mais pas écrire dans le fichier chfn.

passwd a ces permissions : -rw-r--r--  1 root root     2598 janv. 13 15:02
On remarque que le root peut lire et écrire.

## Question 6

Réponse :  Les mots de passe sont stockés dans le répertoire etc/shadow.
Explication :
Avant les mots de passe étaient avec toutes les informations personnelles dans le fichier "psswd" mais on s'est vite rendu compte que ce n'était pas sécurisé mais aussi pas pratique, car poser des restrictions seulement sur les mots de passes était plus compliqué.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








